#本插件是基于vue的滚动条插件 
目前版本是 1.3 
名字叫vuescroll
大家好，还有一个我的开源项目叫bms，是基于vue的，在我的主页上，大家可以去看看。效果图如下，支持一个页面多个元素的滚动。具体实现可到demo中体验。
![运行效果图](https://git.oschina.net/uploads/images/2017/0727/144942_25085871_1380797.png "dd.png")
-------------
 **

### 原理讲解
**
首先，本插件的原理参照了一款jQuery的插件 叫 slimscroll，这是一款非常优秀的jQuery插件。</br>
产生背景：由于被人再做一个基于vue的项目，叫[bms](https://git.oschina.net/yaoliguo/bms)，这个项目前台是基于vue+element的，后台是基于Java的，因为产生菜单的时候发现原生的滚动条太差劲了，又因为在网上没有找到合适的vue滚动条，所以有了做一个vue的滚动条的想法。</br>
原理讲解：很简单，就是通过改变滚动条div的top值来改变滚动内容的scrollTop值。我的QQ 72400354.
 **

### 用法步骤如下
** 

1 引入依赖文件vue.js 和插件vuescroll.js
```
<script src="js/vue.js" type="text/javascript" charset="utf-8"></script>
<script src="js/vuescroll.js" type="text/javascript" charset="utf-8"></script>
``` 

2 用scroll的组件包含你需要滚动的div，并绑定配置选项
```
 <div id="scroll1" >
		 	   <div id="scroll" style="position: relative;">
		 	   	<vue-scrollPanel>
		      		<vue-scroll-con>
		 	   	<div id="content"></div>
		 	   	</vue-scroll-con>
				     </vue-scrollPanel>
				     <vue-scroll :ops="ops"  @scroll="lookscroll">
		  	   
		  	         </vue-scroll>
		 	   </div></div>
```
如图 ： ![输入图片说明](https://git.oschina.net/uploads/images/2017/0720/190131_ed36d6f2_1380797.jpeg "demo1.jpg")
3 根据自己的喜好配置滚动条的参数
```
var aa = new Vue({
			el:'#scroll1',
			data:{
			   ops:{
			   	 background:"#cecece",
			   	  width:'5px',
			   	  deltaY:'100',
			   	  float:'right' 
			   }
			},
			methods:{
			lookscroll:function(bar,content){
				console.log(bar);
				console.log(content);
			}	
				}
			
		});
```
![项目配置如图，新增回调函数](https://git.oschina.net/uploads/images/2017/0727/144505_bf335863_1380797.png "callback.png")
  
V1.3 版本 新增回调函数

4 可以运行体验一下了 